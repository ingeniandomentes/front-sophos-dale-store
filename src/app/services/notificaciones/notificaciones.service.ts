import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class NotificacionesService {
  private Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    width: '330px',
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    },
  });

  constructor() {}

  showSuccess(titulo: string, mensaje: string, time: number): void {
    Swal.fire({
      icon: 'success',
      title: titulo,
      text: mensaje,
      timer: time,
    });
  }

  showError(titulo: string, mensaje: string, time: number): void {
    Swal.fire({
      icon: 'error',
      title: titulo,
      text: mensaje,
      timer: time,
    });
  }

  showWarning(titulo: string, mensaje: string, time: number): void {
    Swal.fire({
      icon: 'warning',
      title: titulo,
      text: mensaje,
      timer: time,
    });
  }

  showInfo(titulo: string, mensaje: string, time: number): void {
    Swal.fire({
      icon: 'info',
      title: titulo,
      text: mensaje,
      timer: time,
    });
  }

  showDesitionTwoButtons(
    titulo: string,
    mensaje: string,
    cancelButtonText: string,
    confirmButtonText: string
  ): Promise<any> {
    return Swal.fire({
      title: titulo,
      text: mensaje,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText,
      cancelButtonText,
    }).then((result) => {
      return result;
    });
  }

  toastShowSuccess(titulo: string, time: number): void {
    this.Toast.fire({
      icon: 'success',
      title: titulo,
      customClass: {
        popup: 'border-toast success',
      },
      timer: time,
    });
  }

  toastShowError(titulo: string, time: number): void {
    this.Toast.fire({
      icon: 'error',
      title: titulo,
      customClass: {
        popup: 'border-toast error',
      },
      timer: time,
    });
  }

  toastShowWarning(titulo: string, time: number): void {
    this.Toast.fire({
      icon: 'warning',
      title: titulo,
      customClass: {
        popup: 'border-toast warning',
      },
      timer: time,
    });
  }

  toastShowInfo(titulo: string, time: number): void {
    this.Toast.fire({
      icon: 'info',
      title: titulo,
      customClass: {
        popup: 'border-toast info',
      },
      timer: time,
    });
  }
}
