import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public token: string = '';

  constructor(private http: HttpClient) {}

  get getDataUser() {
    return this.token;
  }
}
