import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss'],
})
export class LogsComponent implements OnInit {
  public listData: Array<any> = [{}];

  constructor() {
    this.listData = [
      {
        id: 1,
        action: 'GET',
        user: 1,
        created_at: '2022/05/05T08:00',
        modified_at: '2022/05/05T08:00',
      },
      {
        id: 3,
        action: 'POST',
        user: 3,
        created_at: '2022/05/05T08:00',
        modified_at: '2022/05/05T08:00',
      },
      {
        id: 4,
        action: 'POST',
        user: 4,
        created_at: '2022/05/05T08:00',
        modified_at: '2022/05/05T08:00',
      },
      {
        id: 6,
        action: 'PUT',
        user: 4,
        created_at: '2022/05/05T08:00',
        modified_at: '2022/05/05T08:00',
      },
      {
        id: 123,
        action: 'DELETE',
        user: 2,
        created_at: '2022/05/05T08:00',
        modified_at: '2022/05/05T08:00',
      },
    ];
  }

  ngOnInit(): void {}
}
