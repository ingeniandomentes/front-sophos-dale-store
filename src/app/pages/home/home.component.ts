import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public listData: Array<any> = [{}];
  public usuario: string = 'usuario';

  constructor() {
    let userRol = sessionStorage.getItem('rol');
    this.listData = [
      {
        name: 'Productos',
        quantity: 20,
        show: true,
        url_image: '../../assets/img/product.jpg',
      },
      {
        name: 'Dinero',
        quantity: 2000000,
        show: true,
        url_image: '../../assets/img/money.jpg',
        pipe: true,
      },
      {
        name: 'Usuarios',
        quantity: 200,
        show: userRol === 'admin' ? true : false,
        url_image: '../../assets/img/user.jpg',
      },
    ];
  }

  ngOnInit(): void {}
}
