import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public isShowHidePassword: boolean = false;
  public isShowValidations: boolean = false;
  public loginForm: FormGroup = this.formBuilder.group({});

  constructor(
    private formBuilder: FormBuilder,
    private notificacionesService: NotificacionesService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.onLoadForm();
  }

  onLoadForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  onShowHidePassword() {
    this.isShowHidePassword = !this.isShowHidePassword;
  }

  onGetFormValues(value: string) {
    return this.loginForm.get(value);
  }

  onLogin() {
    this.isShowValidations = false;
    if (this.loginForm.valid) {
      sessionStorage.setItem('token', '123456');
      sessionStorage.setItem('rol', 'admin');
      this.notificacionesService.toastShowSuccess('Bienvenido', 2000);
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 2000);
    } else {
      this.isShowValidations = true;
      this.notificacionesService.toastShowError('Error en los campos', 2000);
    }
  }
}
