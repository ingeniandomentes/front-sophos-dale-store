import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoComprasComponent } from './nuevo-compras.component';

describe('NuevoComprasComponent', () => {
  let component: NuevoComprasComponent;
  let fixture: ComponentFixture<NuevoComprasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NuevoComprasComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoComprasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
