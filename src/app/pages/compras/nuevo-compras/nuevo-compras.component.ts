import { CurrencyPipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';

@Component({
  selector: 'app-nuevo-compras',
  templateUrl: './nuevo-compras.component.html',
  styleUrls: ['./nuevo-compras.component.scss'],
})
export class NuevoComprasComponent implements OnInit {
  public listData: Array<any> = [{}];
  public isShowModal: boolean = false;
  public isEndShopping: boolean = false;

  public bodyModal: any = {};

  constructor(
    private notificacionesService: NotificacionesService,
    private router: Router,
    private currencyPipe: CurrencyPipe
  ) {
    this.listData = [
      {
        id: 1,
        name: 'Producto 1',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: false,
        stock: 20,
        quantity: 0,
        unit_price: 2000,
        total_price: 0,
      },
      {
        id: 2,
        name: 'Producto 2',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: false,
        stock: 20,
        quantity: 0,
        unit_price: 2000,
        total_price: 0,
      },
      {
        id: 3,
        name: 'Producto 3',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: false,
        stock: 20,
        quantity: 0,
        unit_price: 2000,
        total_price: 0,
      },
      {
        id: 4,
        name: 'Producto 4',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: false,
        stock: 20,
        quantity: 0,
        unit_price: 2000,
        total_price: 0,
      },
      {
        id: 5,
        name: 'Producto 5',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: false,
        stock: 20,
        quantity: 0,
        unit_price: 2000,
        total_price: 0,
      },
      {
        id: 6,
        name: 'Producto 6',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: false,
        stock: 20,
        quantity: 0,
        unit_price: 2000,
        total_price: 0,
      },
      {
        id: 7,
        name: 'Producto 7',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: false,
        stock: 20,
        quantity: 0,
        unit_price: 2000,
        total_price: 0,
      },
      {
        id: 8,
        name: 'Producto 8',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: false,
        stock: 20,
        quantity: 0,
        unit_price: 2000,
        total_price: 0,
      },
    ];
  }

  ngOnInit(): void {}

  onSelectProduct(id: number) {
    const value = this.listData.find((data) => data.id === id);
    this.listData.find((data) => data.id === id).checked = !value.checked;
    this.listData.find((data) => data.id === id).quantity = 1;
    this.listData.find((data) => data.id === id).total_price =
      value.quantity * value.unit_price;
    if (this.listData.some((data) => data.checked === true)) {
      this.isEndShopping = true;
    } else {
      this.isEndShopping = false;
    }
  }

  onMinusQuantity(id: number) {
    let value = this.listData.find((data) => data.id === id);
    value.quantity--;
    if (value.quantity < 0) {
      this.listData.find((data) => data.id === id).checked = false;
    } else {
      this.listData.find((data) => data.id === id).checked = value.quantity;
      this.listData.find((data) => data.id === id).total_price =
        value.quantity * value.unit_price;
    }
  }

  onPlusQuantity(id: number) {
    let value = this.listData.find((data) => data.id === id);
    if (value.quantity >= value.stock) {
      this.notificacionesService.toastShowError(
        'No puedes agregar valores mayores al stock',
        2000
      );
    } else {
      value.quantity++;
      this.listData.find((data) => data.id === id).quantity = value.quantity;
      this.listData.find((data) => data.id === id).total_price =
        value.quantity * value.unit_price;
    }
  }

  async onConfirmSell() {
    const dataSelected = this.listData.filter((data) => data.checked === true);
    let total_price: number = 0;
    let body: string = '';
    await dataSelected.map((data: any) => {
      body += `
      <tr class="text-center">
        <th scope="row">${data?.name}</th>
        <td>${this.currencyPipe.transform(data?.unit_price, 'USD')}</td>
        <td>${data?.quantity}</td>
        <td>${this.currencyPipe.transform(data?.total_price, 'USD')}</td>
      </tr>
      `;
      total_price += data.total_price;
    });
    const bodyModal = `
      <div class="card text-left">
          <div class="card-body">
            <table class="table table-bordered">
              <thead>
                <tr class="text-center">
                  <th scope="col">Producto</th>
                  <th scope="col">Precio Unitario</th>
                  <th scope="col">Cantidad</th>
                  <th scope="col">Precio Total</th>
                </tr>
              </thead>
              <tbody>
                ${body}
              </tbody>
            </table>
          </div>
          <div class="card-footer text-center">
            Precio Total: <strong>${total_price}</strong>
          </div>
        </div>
    `;
    const objectSended = {
      body: bodyModal,
      accept_button: 'Aceptar',
      close_button: 'Cancelar',
      title: 'Resumen de la compra',
    };
    this.bodyModal = objectSended;
    this.isShowModal = true;
  }

  onAcceptSell() {
    this.isShowModal = false;
    this.notificacionesService.showSuccess(
      'Felicitaciones',
      'Compra realizada exitosamente',
      2000
    );
    setTimeout(() => this.router.navigate(['compras']));
  }

  onCancellSell() {
    this.isShowModal = false;
  }
}
