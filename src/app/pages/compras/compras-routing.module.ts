import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/guards/auth/auth.guard';
import { comprasRoutes } from './compras.routes';
import { RouterModule, Routes } from '@angular/router';
import { ComprasComponent } from './compras.component';

export const routes: Routes = [
  {
    path: '',
    component: ComprasComponent,
    canActivateChild: [AuthGuard],
    children: comprasRoutes,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
})
export class ComprasRoutingModule {}
