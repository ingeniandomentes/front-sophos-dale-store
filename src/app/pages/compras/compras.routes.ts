import { Routes } from '@angular/router';
import { ListaComprasComponent } from './lista-compras/lista-compras.component';
import { NuevoComprasComponent } from './nuevo-compras/nuevo-compras.component';
import { VerComprasComponent } from './ver-compras/ver-compras.component';

export const comprasRoutes: Routes = [
  { path: '', component: ListaComprasComponent },
  { path: 'nuevo', component: NuevoComprasComponent },
  { path: 'ver/:id', component: VerComprasComponent },
];
