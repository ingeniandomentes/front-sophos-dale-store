import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-compras',
  templateUrl: './lista-compras.component.html',
  styleUrls: ['./lista-compras.component.scss'],
})
export class ListaComprasComponent implements OnInit {
  public listData: Array<any> = [{}];

  constructor(private router: Router) {
    this.listData = [
      {
        id: 1,
        usuario: 'usuario',
        total: '200000',
        fecha: '05/12/2021 08:00',
      },
      {
        id: 3,
        usuario: 'usuario',
        total: '200000',
        fecha: '05/12/2021 08:00',
      },
      {
        id: 8,
        usuario: 'usuario',
        total: '200000',
        fecha: '05/12/2021 08:00',
      },
      {
        id: 14,
        usuario: 'usuario',
        total: '200000',
        fecha: '05/12/2021 08:00',
      },
    ];
  }

  ngOnInit(): void {}

  onDetail(id: any) {
    this.router.navigate(['compras/ver/' + id]);
  }
}
