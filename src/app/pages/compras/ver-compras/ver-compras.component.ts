import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ver-compras',
  templateUrl: './ver-compras.component.html',
  styleUrls: ['./ver-compras.component.scss'],
})
export class VerComprasComponent implements OnInit {
  public id: any;
  public listData: Array<any> = [{}];
  public total_price: number = 0;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.listData = [
      {
        id: 5,
        name: 'Producto 5',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: true,
        stock: 20,
        quantity: 3,
        unit_price: 2000,
        total_price: 6000,
      },
      {
        id: 7,
        name: 'Producto 7',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: true,
        stock: 20,
        quantity: 3,
        unit_price: 2000,
        total_price: 6000,
      },
      {
        id: 8,
        name: 'Producto 8',
        description: 'Esta es la descripción del producto',
        url: '../../assets/img/product.jpg',
        checked: true,
        stock: 20,
        quantity: 2,
        unit_price: 2000,
        total_price: 4000,
      },
    ];
    this.listData.map((data) => {
      this.total_price += data.total_price;
    });
  }
}
