import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComprasRoutingModule } from './compras-routing.module';

import { VerComprasComponent } from './ver-compras/ver-compras.component';
import { NuevoComprasComponent } from './nuevo-compras/nuevo-compras.component';
import { ListaComprasComponent } from './lista-compras/lista-compras.component';
import { MenuModule } from 'src/app/components/menu/menu.module';

@NgModule({
  declarations: [
    VerComprasComponent,
    ListaComprasComponent,
    NuevoComprasComponent,
  ],
  imports: [CommonModule, RouterModule, ComprasRoutingModule, MenuModule],
})
export class ComprasModule {}
