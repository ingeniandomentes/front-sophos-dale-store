import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-page404',
  templateUrl: './page404.component.html',
  styleUrls: ['./page404.component.scss'],
})
export class Page404Component implements OnInit {
  public link: string = '';

  constructor(private router: Router, private authService: AuthService) {
    this.authService.token = JSON.stringify(sessionStorage.getItem('token'));
  }

  ngOnInit(): void {
    if (this.authService.token !== '' && this.authService.token !== 'null') {
      this.link = '/';
    } else {
      this.link = '/login';
    }
  }
}
