import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/guards/auth/auth.guard';
import { usuariosRoutes } from './usuarios.routes';
import { RouterModule, Routes } from '@angular/router';
import { UsuariosComponent } from './usuarios.component';

export const routes: Routes = [
  {
    path: '',
    component: UsuariosComponent,
    canActivateChild: [AuthGuard],
    children: usuariosRoutes,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
})
export class UsuariosRoutingModule {}
