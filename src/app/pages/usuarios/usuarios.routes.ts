import { Routes } from '@angular/router';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { NuevoUsuariosComponent } from './nuevo-usuarios/nuevo-usuarios.component';
import { EditarUsuariosComponent } from './editar-usuarios/editar-usuarios.component';
import { RolGuard } from 'src/app/guards/rol/rol.guard';

export const usuariosRoutes: Routes = [
  { path: '', component: ListaUsuariosComponent, canActivate: [RolGuard] },
  { path: 'nuevo', component: NuevoUsuariosComponent, canActivate: [RolGuard] },
  {
    path: 'editar/:id',
    component: EditarUsuariosComponent,
    canActivate: [RolGuard],
  },
];
