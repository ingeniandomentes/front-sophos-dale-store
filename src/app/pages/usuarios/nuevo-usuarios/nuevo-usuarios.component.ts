import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';
import { checkPasswords } from 'src/app/utils/custom-validators/checkPassword';

@Component({
  selector: 'app-nuevo-usuarios',
  templateUrl: './nuevo-usuarios.component.html',
  styleUrls: ['./nuevo-usuarios.component.scss'],
})
export class NuevoUsuariosComponent implements OnInit {
  public isShowHidePassword: boolean = false;
  public isShowHideRePassword: boolean = false;
  public isShowValidations: boolean = false;
  public newUserForm: FormGroup = this.formBuilder.group({});

  constructor(
    private formBuilder: FormBuilder,
    private notificacionesService: NotificacionesService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.onLoadForm();
  }

  onLoadForm() {
    this.newUserForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      username: ['', [Validators.required]],
      cedula: ['', [Validators.required]],
      telefono: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [Validators.required, checkPasswords('re_password', true)],
      ],
      re_password: ['', [Validators.required, checkPasswords('password')]],
    });
  }

  onShowHidePassword() {
    this.isShowHidePassword = !this.isShowHidePassword;
  }

  onShowHideRePassword() {
    this.isShowHideRePassword = !this.isShowHideRePassword;
  }

  onGetFormValues(value: string) {
    return this.newUserForm.get(value);
  }

  onRegister() {
    this.isShowValidations = false;
    if (this.newUserForm.valid) {
      this.notificacionesService.toastShowSuccess(
        'Usuario creado exitosamente',
        2000
      );
      setTimeout(() => {
        this.router.navigate(['/usuarios']);
      }, 2000);
    } else {
      this.isShowValidations = true;
      this.notificacionesService.toastShowError('Error en los campos', 2000);
    }
  }
}
