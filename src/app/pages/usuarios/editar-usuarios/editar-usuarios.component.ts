import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';
import { checkPasswords } from 'src/app/utils/custom-validators/checkPassword';

@Component({
  selector: 'app-editar-usuarios',
  templateUrl: './editar-usuarios.component.html',
  styleUrls: ['./editar-usuarios.component.scss'],
})
export class EditarUsuariosComponent implements OnInit {
  public id: any;
  public isShowHidePassword: boolean = false;
  public isShowHideRePassword: boolean = false;
  public isShowValidations: boolean = false;
  public editUserForm: FormGroup = this.formBuilder.group({});

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private notificacionesService: NotificacionesService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.onLoadForm();
  }

  onLoadForm() {
    this.editUserForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      username: ['', [Validators.required]],
      cedula: ['', [Validators.required]],
      telefono: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: [
        '',
        [Validators.required, checkPasswords('re_password', true)],
      ],
      re_password: ['', [Validators.required, checkPasswords('password')]],
    });
  }

  onShowHidePassword() {
    this.isShowHidePassword = !this.isShowHidePassword;
  }

  onShowHideRePassword() {
    this.isShowHideRePassword = !this.isShowHideRePassword;
  }

  onGetFormValues(value: string) {
    return this.editUserForm.get(value);
  }

  onEdit() {
    this.isShowValidations = false;
    if (this.editUserForm.valid) {
      this.notificacionesService.toastShowSuccess(
        'Usuario editado exitosamente',
        2000
      );
      setTimeout(() => {
        this.router.navigate(['/usuarios']);
      }, 2000);
    } else {
      this.isShowValidations = true;
      this.notificacionesService.toastShowError('Error en los campos', 2000);
    }
  }
}
