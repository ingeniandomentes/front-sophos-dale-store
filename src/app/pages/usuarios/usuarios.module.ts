import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UsuariosRoutingModule } from './usuarios-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NuevoUsuariosComponent } from './nuevo-usuarios/nuevo-usuarios.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { EditarUsuariosComponent } from './editar-usuarios/editar-usuarios.component';
import { MenuModule } from 'src/app/components/menu/menu.module';

@NgModule({
  declarations: [
    NuevoUsuariosComponent,
    ListaUsuariosComponent,
    EditarUsuariosComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    UsuariosRoutingModule,
    MenuModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class UsuariosModule {}
