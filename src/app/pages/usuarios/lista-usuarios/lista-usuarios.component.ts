import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.scss'],
})
export class ListaUsuariosComponent implements OnInit {
  public listData: Array<any> = [{}];

  constructor(
    private router: Router,
    private notificacionesService: NotificacionesService
  ) {
    this.listData = [
      {
        id: 1,
        username: 'usuario',
        name: 'Usuario 1',
        email: 'user@test.com',
      },
      {
        id: 4,
        username: 'usuario',
        name: 'Usuario 4',
        email: 'user@test.com',
      },
      {
        id: 8,
        username: 'usuario',
        name: 'Usuario 8',
        email: 'user@test.com',
      },
      {
        id: 16,
        username: 'usuario',
        name: 'Usuario 16',
        email: 'user@test.com',
      },
    ];
  }

  ngOnInit(): void {}

  onEdit(id: any) {
    this.router.navigate(['usuarios/editar/' + id]);
  }

  onDelete(id: any) {
    this.notificacionesService
      .showDesitionTwoButtons(
        'Advertencia',
        `¿Estas seguro que deseas eliminar el usuario #${id}?`,
        'Cancelar',
        'Aceptar'
      )
      .then((res) => {
        if (res?.isConfirmed) {
          this.notificacionesService.toastShowSuccess(
            'Usuario eliminado exitosamente',
            2000
          );
        }
      });
  }
}
