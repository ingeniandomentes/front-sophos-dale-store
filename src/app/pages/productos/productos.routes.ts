import { Routes } from '@angular/router';
import { ListaProductosComponent } from './lista-productos/lista-productos.component';
import { NuevoProductosComponent } from './nuevo-productos/nuevo-productos.component';
import { EditarProductosComponent } from './editar-productos/editar-productos.component';
import { RolGuard } from 'src/app/guards/rol/rol.guard';

export const productosRoutes: Routes = [
  { path: '', component: ListaProductosComponent, canActivate: [RolGuard] },
  {
    path: 'nuevo',
    component: NuevoProductosComponent,
    canActivate: [RolGuard],
  },
  {
    path: 'editar/:id',
    component: EditarProductosComponent,
    canActivate: [RolGuard],
  },
];
