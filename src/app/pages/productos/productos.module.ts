import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProductosRoutingModule } from './productos-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NuevoProductosComponent } from './nuevo-productos/nuevo-productos.component';
import { ListaProductosComponent } from './lista-productos/lista-productos.component';
import { EditarProductosComponent } from './editar-productos/editar-productos.component';
import { MenuModule } from 'src/app/components/menu/menu.module';

@NgModule({
  declarations: [
    NuevoProductosComponent,
    ListaProductosComponent,
    EditarProductosComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ProductosRoutingModule,
    MenuModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class ProductosModule {}
