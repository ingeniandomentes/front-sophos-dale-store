import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';
import { checkPasswords } from 'src/app/utils/custom-validators/checkPassword';
import { DataService } from 'src/app/services/data/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-editar-productos',
  templateUrl: './editar-productos.component.html',
  styleUrls: ['./editar-productos.component.scss'],
})
export class EditarProductosComponent implements OnInit {
  public id: any;
  public isShowHidePassword: boolean = false;
  public isShowHideRePassword: boolean = false;
  public isShowValidations: boolean = false;
  public editProductoForm: FormGroup = this.formBuilder.group({});
  public isShowForm: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private notificacionesService: NotificacionesService,
    private router: Router,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataService
      .get(`${environment.API_URL}productos/${this.id}`)
      .subscribe((res) => {
        this.onSetValuesForm(res);
      });
    this.onLoadForm();
  }

  onLoadForm() {
    this.editProductoForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      url: ['', [Validators.required]],
      stock: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
      unit_price: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
    });
  }

  onSetValuesForm(values: any) {
    this.editProductoForm.patchValue({
      name: values.name,
      description: values.description,
      url: values.url,
      stock: values.stock,
      unit_price: values.unit_price,
    });
    this.isShowForm = true;
  }

  onGetFormValues(value: string) {
    return this.editProductoForm.get(value);
  }

  onEdit() {
    this.isShowValidations = false;
    if (this.editProductoForm.valid) {
      const body = {
        name: this.onGetFormValues('name')?.value,
        description: this.onGetFormValues('description')?.value,
        url: this.onGetFormValues('url')?.value,
        unit_price: this.onGetFormValues('unit_price')?.value,
        stock: this.onGetFormValues('stock')?.value,
      };
      this.dataService
        .put(`${environment.API_URL}productos/${this.id}`, body)
        .subscribe(() => {
          this.notificacionesService.toastShowSuccess(
            'Producto editado exitosamente',
            2000
          );
          setTimeout(() => {
            this.router.navigate(['/productos']);
          }, 2000);
        });
    } else {
      this.isShowValidations = true;
      this.notificacionesService.toastShowError('Error en los campos', 2000);
    }
  }
}
