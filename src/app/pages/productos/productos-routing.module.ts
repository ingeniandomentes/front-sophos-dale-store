import { NgModule } from '@angular/core';
import { AuthGuard } from 'src/app/guards/auth/auth.guard';
import { productosRoutes } from './productos.routes';
import { RouterModule, Routes } from '@angular/router';
import { ProductosComponent } from './productos.component';

export const routes: Routes = [
  {
    path: '',
    component: ProductosComponent,
    canActivateChild: [AuthGuard],
    children: productosRoutes,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
})
export class ProductosRoutingModule {}
