import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data/data.service';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';
import { checkPasswords } from 'src/app/utils/custom-validators/checkPassword';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-nuevo-productos',
  templateUrl: './nuevo-productos.component.html',
  styleUrls: ['./nuevo-productos.component.scss'],
})
export class NuevoProductosComponent implements OnInit {
  public isShowHidePassword: boolean = false;
  public isShowHideRePassword: boolean = false;
  public isShowValidations: boolean = false;
  public newProductoForm: FormGroup = this.formBuilder.group({});

  constructor(
    private formBuilder: FormBuilder,
    private notificacionesService: NotificacionesService,
    private router: Router,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.onLoadForm();
  }

  onLoadForm() {
    this.newProductoForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      url: ['', [Validators.required]],
      stock: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
      unit_price: ['', [Validators.required, Validators.pattern(/^[0-9]*$/)]],
    });
  }

  onGetFormValues(value: string) {
    return this.newProductoForm.get(value);
  }

  onRegister() {
    this.isShowValidations = false;
    if (this.newProductoForm.valid) {
      const body = {
        name: this.onGetFormValues('name')?.value,
        description: this.onGetFormValues('description')?.value,
        url: this.onGetFormValues('url')?.value,
        unit_price: this.onGetFormValues('unit_price')?.value,
        stock: this.onGetFormValues('stock')?.value,
      };
      this.dataService
        .post(`${environment.API_URL}productos`, body)
        .subscribe(() => {
          this.notificacionesService.toastShowSuccess(
            'Producto creado exitosamente',
            2000
          );
          setTimeout(() => {
            this.router.navigate(['/productos']);
          }, 2000);
        });
    } else {
      this.isShowValidations = true;
      this.notificacionesService.toastShowError('Error en los campos', 2000);
    }
  }
}
