import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data/data.service';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.scss'],
})
export class ListaProductosComponent implements OnInit {
  public listData: Array<any> = [{}];
  public isShowTable: boolean = false;

  constructor(
    private router: Router,
    private notificacionesService: NotificacionesService,
    private dataService: DataService
  ) {
    /*this.listData = [
      {
        id: 1,
        name: 'Producto',
        description: 'Descripción producto',
        url: '../../assets/img/product.jpg',
        unit_price: 2000,
        stock: 20,
      },
      {
        id: 4,
        name: 'Producto',
        description: 'Descripción producto',
        url: '../../assets/img/product.jpg',
        unit_price: 2000,
        stock: 20,
      },
      {
        id: 8,
        name: 'Producto',
        description: 'Descripción producto',
        url: '../../assets/img/product.jpg',
        unit_price: 2000,
        stock: 20,
      },
      {
        id: 15,
        name: 'Producto',
        description: 'Descripción producto',
        url: '../../assets/img/product.jpg',
        unit_price: 2000,
        stock: 20,
      },
      {
        id: 20,
        name: 'Producto',
        description: 'Descripción producto',
        url: '../../assets/img/product.jpg',
        unit_price: 2000,
        stock: 20,
      },
    ];*/
  }

  ngOnInit(): void {
    this.onGetInitialData();
  }

  async onGetInitialData() {
    this.listData = [];
    this.dataService.get(`${environment.API_URL}productos`).subscribe((res) => {
      this.listData = res;
      this.isShowTable = true;
    });
  }

  onEdit(id: any) {
    this.router.navigate(['productos/editar/' + id]);
  }

  async onDelete(id: any) {
    await this.notificacionesService
      .showDesitionTwoButtons(
        'Advertencia',
        `¿Estas seguro que deseas eliminar el producto #${id}?`,
        'Cancelar',
        'Aceptar'
      )
      .then((res: any) => {
        if (res?.isConfirmed) {
          this.isShowTable = false;
          this.dataService
            .delete(`${environment.API_URL}productos/${id}`)
            .subscribe(() => {
              this.notificacionesService.toastShowSuccess(
                'Producto eliminado exitosamente',
                2000
              );
              setTimeout(async () => {
                await this.onGetInitialData();
                this.isShowTable = true;
              }, 500);
            });
        }
      });
  }
}
