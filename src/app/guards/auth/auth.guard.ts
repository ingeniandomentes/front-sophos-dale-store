import { Injectable } from '@angular/core';
import { CanActivateChild, CanLoad, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanLoad, CanActivateChild {
  constructor(private router: Router, private authService: AuthService) {
    this.authService.token = JSON.stringify(sessionStorage.getItem('token'));
  }

  canLoad(): boolean {
    if (this.authService.token === '' || this.authService.token === 'null') {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

  canActivateChild(): boolean {
    if (this.authService.token === '' || this.authService.token === 'null') {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
