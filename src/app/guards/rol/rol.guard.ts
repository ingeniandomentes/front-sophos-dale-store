import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class RolGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthService) {
    this.authService.token = JSON.stringify(sessionStorage.getItem('token'));
  }

  canActivate() {
    const roles = sessionStorage.getItem('rol');
    console.log(roles);
    if (roles !== 'admin') {
      return false;
    }
    return true;
  }
}
