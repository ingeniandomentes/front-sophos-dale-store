import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() public info: any = {
    title: 'Titulo',
    body: `<h1>Cuerpo</h1>`,
    accept_button: 'Aceptar',
    close_button: 'Cancelar',
  };

  @Output() cancelAction: EventEmitter<any> = new EventEmitter();
  @Output() acceptAction: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onCancelAction() {
    this.cancelAction.emit();
  }

  onAcceptAction() {
    this.acceptAction.emit();
  }
}
