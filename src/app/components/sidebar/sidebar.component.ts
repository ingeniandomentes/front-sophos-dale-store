import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificacionesService } from 'src/app/services/notificaciones/notificaciones.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  public listMenus: Array<any> = [{}];

  constructor(
    private notificacionesService: NotificacionesService,
    private router: Router
  ) {
    let userRole = sessionStorage.getItem('rol');
    console.log(userRole);
    this.listMenus = [
      {
        url: '/',
        exact: true,
        icon: 'fas fa-home',
        title: 'Inicio',
        show: true,
      },
      {
        url: '/compras',
        exact: true,
        icon: 'fas fa-money-bill-wave',
        title: 'Compras',
        show: true,
      },
      {
        url: '/usuarios',
        exact: true,
        icon: 'fas fa-user',
        title: 'Usuarios',
        show: userRole === 'admin' ? true : false,
      },
      {
        url: '/productos',
        exact: true,
        icon: 'fas fa-shopping-cart',
        title: 'Productos',
        show: userRole === 'admin' ? true : false,
      },
      {
        url: '/logs',
        exact: true,
        icon: 'fas fa-tasks',
        title: 'Logs',
        show: userRole === 'admin' ? true : false,
      },
    ];
  }

  ngOnInit(): void {}

  onLogOut() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('rol');
    this.notificacionesService.toastShowSuccess('Adios', 2000);
    setTimeout(() => {
      this.router.navigate(['/login']);
    }, 2000);
  }
}
