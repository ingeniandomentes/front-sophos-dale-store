import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MenuRoutingModule } from './menu-routing.module';

import { MenuComponent } from './menu.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { HomeComponent } from 'src/app/pages/home/home.component';
import { ProductosComponent } from 'src/app/pages/productos/productos.component';
import { LogsComponent } from 'src/app/pages/logs/logs.component';
import { ComprasComponent } from 'src/app/pages/compras/compras.component';
import { UsuariosComponent } from 'src/app/pages/usuarios/usuarios.component';
import { ModalComponent } from '../modal/modal.component';

@NgModule({
  declarations: [
    MenuComponent,
    SidebarComponent,
    HomeComponent,
    ProductosComponent,
    LogsComponent,
    ComprasComponent,
    UsuariosComponent,
    ModalComponent,
  ],
  imports: [CommonModule, RouterModule, MenuRoutingModule],
  exports: [ModalComponent],
})
export class MenuModule {}
