import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth/auth.guard';
import { MenuComponent } from './menu.component';
import { menuRoutes } from './menu.routes';

export const routes: Routes = [
  {
    path: '',
    component: MenuComponent,
    canActivateChild: [AuthGuard],
    children: menuRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuRoutingModule {}
