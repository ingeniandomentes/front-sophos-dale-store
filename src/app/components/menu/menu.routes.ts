import { Routes } from '@angular/router';
import { RolGuard } from 'src/app/guards/rol/rol.guard';
import { HomeComponent } from 'src/app/pages/home/home.component';
import { LogsComponent } from 'src/app/pages/logs/logs.component';

export const menuRoutes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'usuarios',
    loadChildren: () =>
      import('../../pages/usuarios/usuarios.module').then(
        (a) => a.UsuariosModule
      ),
  },
  {
    path: 'productos',
    loadChildren: () =>
      import('../../pages/productos/productos.module').then(
        (a) => a.ProductosModule
      ),
  },
  {
    path: 'compras',
    loadChildren: () =>
      import('../../pages/compras/compras.module').then((a) => a.ComprasModule),
  },
  { path: 'logs', component: LogsComponent, canActivate: [RolGuard] },
];
